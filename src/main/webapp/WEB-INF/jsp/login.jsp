<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <h1>Login</h1>
        <c:if test="${param.error != null}">
            <h3>Invalid username or password. Please try again.</h3>
        </c:if>
        <form action="<c:url value="/login"/>" method="post">
            Username: <input type="text" name="username" placeholder="Username" value="zim"/>
            <br/>
            Password: <input type="password" name="password" placeholder="Password" value="demo"/>
            <br/>
            <input type="submit" value="Login"/>
        </form>
    </body>
</html>
