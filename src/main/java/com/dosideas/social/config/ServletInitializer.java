package com.dosideas.social.config;

import com.dosideas.social.ApplicationConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Esta es la clase que inicializa la aplicacion web con Spring Boot.
 * Levanta la clase de configuración principal de la aplicación.
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationConfig.class);
    }

}
