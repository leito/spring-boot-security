# README #

Some simple examples using Spring Boot and Spring Security.

### Available examples in branches ###

* master: a simple example with jdbcAuthentication.
* authentication-with-token: extends the master example adding optional authentication with a token.
